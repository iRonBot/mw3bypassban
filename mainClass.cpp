#include <ctime>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <limits.h>
#include <iostream>
#include <windows.h>

using namespace std;

typedef __int8 int8;
typedef __int16 int16;
typedef __int32 int32;
typedef __int64 int64;
typedef unsigned __int8 uint8;
typedef unsigned __int16 uint16;
typedef unsigned __int32 uint32;
typedef unsigned __int64 uint64;

typedef vector<uint8> BufferType; 

#define JMP_HOOK_SIZE 5
#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

typedef LONG (WINAPI *pZwQuerySystemInformation)(
  DWORD SystemInformationClass,
  PVOID SystemInformation,
  ULONG SystemInformationLength,
  PULONG ReturnLength
);

#define SYSTEMINFO_CLASS_FIRM_TABLE 0x4C
#define SMBIOS_STRUCTURE_SEPARATOR_SIZE 2
#define SMBIOS_STRUCTURE_SEPARATOR "\0\0"
#define SMBIOS_TABLE_SIGNATURE 0x52534D42


#define XNADDR_LEN 0x1C
#define PLAYER_NAME_MAXLEN 0x0C
#define ADDRESS_XUID 0x05A7B1D8
#define ADDRESS_PLAYER_INFO 0x05CCB138
#define ADDRESS_PLAYER_STATS 0x01CDAFBC
#define ADDRESS_XNADDRESS_BUFFER 0x00464A58
#define ADDRESS_UNNAMEDPLAYER_NAME 0x007E5AC4

#define RAND_STR_MASK_LEN 62
static const char *RAND_STR_MASK = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


//Mersienne Twister pseudorandom number generator
class MTRand {
// Data
public:
    enum { N = 624 };       // length of state vector
    enum { SAVE = N + 1 };  // length of array for save()

protected:
    enum { M = 397 };  // period parameter

    uint32 state[N];   // internal state
    uint32 *pNext;     // next value to get from state
    int left;          // number of values left before reload needed

//Methods
public:
    MTRand( const uint32& oneSeed );  // initialize with a simple uint32
    MTRand( uint32 *const bigSeed, uint32 const seedLength = N );  // or an array
    MTRand();                         // auto-initialize with /dev/urandom or time() and clock()
    MTRand(const MTRand&);            // prevent copy constructor
    MTRand& operator=(const MTRand&); // no-op operator=

    // Do NOT use for CRYPTOGRAPHY without securely hashing several returned
    // values together, otherwise the generator state can be learned after
    // reading 624 consecutive values.

    // Access to 32-bit random numbers
    double rand();                          // real number in [0,1]
    double rand( const double& n );         // real number in [0,n]
    double randExc();                       // real number in [0,1)
    double randExc( const double& n );      // real number in [0,n)
    double randDblExc();                    // real number in (0,1)
    double randDblExc( const double& n );   // real number in (0,n)
    uint32 randInt();                       // integer in [0,2^32-1]
    uint32 randInt( const uint32& n );      // integer in [0,n] for n < 2^32
    double operator()() { return rand(); }  // same as rand()

    // Access to 53-bit random numbers (capacity of IEEE double precision)
    double rand53();  // real number in [0,1)

    // Access to nonuniform random number distributions
    double randNorm( const double& mean = 0.0, const double& variance = 0.0 );

    // Re-seeding functions with same behavior as initializers
    void seed( const uint32 oneSeed );
    void seed( uint32 *const bigSeed, const uint32 seedLength = N );
    void seed();

    // Saving and loading generator state
    void save( uint32* saveArray ) const;  // to array of size SAVE
    void load( uint32 *const loadArray );  // from such array
    /* Trinity not use streams for random values output
    friend std::ostream& operator<<( std::ostream& os, const MTRand& mtrand );
    friend std::istream& operator>>( std::istream& is, MTRand& mtrand );
    */
protected:
    void initialize( const uint32 oneSeed );
    void reload();
    uint32 hiBit( const uint32& u ) const { return u & 0x80000000UL; }
    uint32 loBit( const uint32& u ) const { return u & 0x00000001UL; }
    uint32 loBits( const uint32& u ) const { return u & 0x7fffffffUL; }
    uint32 mixBits( const uint32& u, const uint32& v ) const
        { return hiBit(u) | loBits(v); }
    uint32 twist( const uint32& m, const uint32& s0, const uint32& s1 ) const
        { return m ^ (mixBits(s0,s1)>>1) ^ uint32(-(int32)(loBit(s1) & 0x9908b0dfUL)); }
    static uint32 hash( time_t t, clock_t c );
};

inline MTRand::MTRand(const MTRand&)
    { seed(); }

inline MTRand& MTRand::operator=(const MTRand&)
    { return *this; }

inline MTRand::MTRand( const uint32& oneSeed )
    { seed(oneSeed); }

inline MTRand::MTRand( uint32 *const bigSeed, const uint32 seedLength )
    { seed(bigSeed,seedLength); }

inline MTRand::MTRand()
    { seed(); }

inline double MTRand::rand()
    { return double(randInt()) * (1.0/4294967295.0); }

inline double MTRand::rand( const double& n )
    { return rand() * n; }

inline double MTRand::randExc()
    { return double(randInt()) * (1.0/4294967296.0); }

inline double MTRand::randExc( const double& n )
    { return randExc() * n; }

inline double MTRand::randDblExc()
    { return ( double(randInt()) + 0.5 ) * (1.0/4294967296.0); }

inline double MTRand::randDblExc( const double& n )
    { return randDblExc() * n; }

inline double MTRand::rand53()
{
    uint32 a = randInt() >> 5, b = randInt() >> 6;
    return ( a * 67108864.0 + b ) * (1.0/9007199254740992.0);  // by Isaku Wada
}

inline double MTRand::randNorm( const double& mean, const double& variance )
{
    // Return a real number from a normal (Gaussian) distribution with given
    // mean and variance by Box-Muller method
    double r = sqrt( -2.0 * log( 1.0-randDblExc()) ) * variance;
    double phi = 2.0 * 3.14159265358979323846264338328 * randExc();
    return mean + r * cos(phi);
}

inline uint32 MTRand::randInt()
{
    // Pull a 32-bit integer from the generator state
    // Every other access function simply transforms the numbers extracted here

    if( left == 0 ) reload();
    --left;

    register uint32 s1;
    s1 = *pNext++;
    s1 ^= (s1 >> 11);
    s1 ^= (s1 <<  7) & 0x9d2c5680UL;
    s1 ^= (s1 << 15) & 0xefc60000UL;
    return ( s1 ^ (s1 >> 18) );
}

inline uint32 MTRand::randInt( const uint32& n )
{
    // Find which bits are used in n
    // Optimized by Magnus Jonsson (magnus@smartelectronix.com)
    uint32 used = n;
    used |= used >> 1;
    used |= used >> 2;
    used |= used >> 4;
    used |= used >> 8;
    used |= used >> 16;

    // Draw numbers until one is found in [0,n]
    uint32 i;
    do
        i = randInt() & used;  // toss unused bits to shorten search
    while( i > n );
    return i;
}

inline void MTRand::seed( const uint32 oneSeed )
{
    // Seed the generator with a simple uint32
    initialize(oneSeed);
    reload();
}

inline void MTRand::seed( uint32 *const bigSeed, const uint32 seedLength )
{
    // Seed the generator with an array of uint32's
    // There are 2^19937-1 possible initial states.  This function allows
    // all of those to be accessed by providing at least 19937 bits (with a
    // default seed length of N = 624 uint32's).  Any bits above the lower 32
    // in each element are discarded.
    // Just call seed() if you want to get array from /dev/urandom
    initialize(19650218UL);
    register int i = 1;
    register uint32 j = 0;
    register int k = ( N > int(seedLength) ? N : int(seedLength) );
    for (; k; --k )
    {
        state[i] =
            state[i] ^ ( (state[i-1] ^ (state[i-1] >> 30)) * 1664525UL );
        state[i] += ( bigSeed[j] & 0xffffffffUL ) + j;
        state[i] &= 0xffffffffUL;
        ++i;  ++j;
        if( i >= N ) { state[0] = state[N-1];  i = 1; }
        if( j >= seedLength ) j = 0;
    }
    for (k = N - 1; k; --k )
    {
        state[i] =
            state[i] ^ ( (state[i-1] ^ (state[i-1] >> 30)) * 1566083941UL );
        state[i] -= i;
        state[i] &= 0xffffffffUL;
        ++i;
        if( i >= N ) { state[0] = state[N-1];  i = 1; }
    }
    state[0] = 0x80000000UL;  // MSB is 1, assuring non-zero initial array
    reload();
}

inline void MTRand::seed()
{
    // Seed the generator with hash of time() and clock() values
    seed( hash( time(NULL), clock() ) );
}

inline void MTRand::initialize( const uint32 seed )
{
    // Initialize generator state with seed
    // See Knuth TAOCP Vol 2, 3rd Ed, p.106 for multiplier.
    // In previous versions, most significant bits (MSBs) of the seed affect
    // only MSBs of the state array.  Modified 9 Jan 2002 by Makoto Matsumoto.
    register uint32 *s = state;
    register uint32 *r = state;
    register int i = 1;
    *s++ = seed & 0xffffffffUL;
    for (; i < N; ++i )
    {
        *s++ = ( 1812433253UL * ( *r ^ (*r >> 30) ) + i ) & 0xffffffffUL;
        r++;
    }
}

inline void MTRand::reload()
{
    // Generate N new values in state
    // Made clearer and faster by Matthew Bellew (matthew.bellew@home.com)
    register uint32 *p = state;
    register int i;
    for (i = N - M; i--; ++p )
        *p = twist( p[M], p[0], p[1] );
    for (i = M; --i; ++p )
        *p = twist( p[M-N], p[0], p[1] );
    *p = twist( p[M-N], p[0], state[0] );

    left = N, pNext = state;
}

inline uint32 MTRand::hash( time_t t, clock_t c )
{
    // Get a uint32 from t and c
    // Better than uint32(x) in case x is floating point in [0,1]
    // Based on code by Lawrence Kirby (fred@genesis.demon.co.uk)

    static uint32 differ = 0;  // guarantee time-based seeds will change

    uint32 h1 = 0;
    unsigned char *p = (unsigned char *) &t;
    for (size_t i = 0; i < sizeof(t); ++i )
    {
        h1 *= UCHAR_MAX + 2U;
        h1 += p[i];
    }
    uint32 h2 = 0;
    p = (unsigned char *) &c;
    for (size_t j = 0; j < sizeof(c); ++j )
    {
        h2 *= UCHAR_MAX + 2U;
        h2 += p[j];
    }
    return ( h1 + differ++ ) ^ h2;
}

inline void MTRand::save( uint32* saveArray ) const
{
    register uint32 *sa = saveArray;
    register const uint32 *s = state;
    register int i = N;
    for (; i--; *sa++ = *s++ ) {}
    *sa = left;
}

inline void MTRand::load( uint32 *const loadArray )
{
    register uint32 *s = state;
    register uint32 *la = loadArray;
    register int i = N;
    for (; i--; *s++ = *la++ ) {}
    left = *la;
    pNext = &state[N-left];
}

static MTRand mtRandom;
pZwQuerySystemInformation ZwQuerySystemInformation;


//SMBIOS Raw Data changer
namespace SMBIOS
{
  
  typedef struct _StructureHeader
  {
    uint8 Type;
    uint8 FormattedSize;
    uint16 Handle; //Unique handle for this structure for later recall
    uint8 Data[];
  }StructureHeader;
  
  enum StructureType
  {
    BIOS_INFO_TYPE = 0x00,
    SYSTEM_INFO_TYPE = 0x01,
    BASEBOARD_INFO_TYPE = 0x02,
    CHASSIS_INFO_TYPE = 0x03,
    PROCESSOR_INFO_TYPE = 0x04,
    CACHE_INFO_TYPE = 0x07,
    PORTS_INFO_TYPE = 0x08,
    SYSTEMSLOTS_INFO_TYPE = 0x09,
    ONBOARDDEVS_INFO_TYPE = 0x0A,
    OEMSTRING_INFO_TYPE = 0x0B,
    SYSTEMCONFIG_INFO_TYPE = 0x0C,
    BIOSLANG_INFO_TYPE = 0x0D,
    GROUPASSOCS_INFO_TYPE = 0x0E,
    SYSLOG_INFO_TYPE = 0x0F,
    PHYSMEM_INFO_TYPE = 0x10,
    MEMDEV_INFO_TYPE = 0x11,
    MEMERROR32_INFO_TYPE = 0x12,
    MEMARRAYMAPPED_INFO_TYPE = 0x13,
    MEMDEVMAPPED_INFO_TYPE = 0x14,
    BUILTINPTRDEV_INFO_TYPE = 0x15,
    BATTERY_INFO_TYPE = 0x16,
    SYSRESET_INFO_TYPE = 0x17,
    HARDSEC_INFO_TYPE = 0x18,
    SYSPOWER_INFO_TYPE = 0x19,
    VOLTPROBE_INFO_TYPE = 0x1A,
    COOLINGDEV_INFO_TYPE = 0x1B,
    TEMPPROBE_INFO_TYPE = 0x1C,
    ELECPROBE_INFO_TYPE = 0x1D,
    OOBRA_INFO_TYPE = 0x1E,
    SYSBOOT_INFO_TYPE = 0x20,
    MEMERROR64_INFO_TYPE = 0x21,
    MNGDEV_INFO_TYPE = 0x22,
    MNGDEVCOMP_INFO_TYPE = 0x23,
    MNGDEVTHRES_INFO_TYPE = 0x24,
    MEMCHAN_INFO_TYPE = 0x25,
    IPMIDEV_INFO_TYPE = 0x26,
    POWERSUPPLY_INFO_TYPE = 0x27,
    ADDITIONAL_INFO_TYPE = 0x28,
    ONBOARDDEVSEX_INFO_TYPE = 0x29,
    MNGCTRLHOSTIF_INFO_TYPE = 0x2A,
    INACTIVE_INFO_TYPE = 0x7E,
    EOF_INFO_TYPE = 0x7F,
  };

  class AlterInfo
  {
    public:
      AlterInfo(uint8 *_buffer, uint32 _size) : m_Buffer(_buffer), m_BufferSize(_size), m_BufferPtr(sizeof(uint32) + sizeof(uint32)){} //Skip version and length
            
      void Process( void )
      {           
        int32 currentStLen = 0;
        while((currentStLen = GetStructureLen()) > (SMBIOS_STRUCTURE_SEPARATOR_SIZE+sizeof(uint32)))
        {
          StructureHeader* pHeader = ((StructureHeader *)&m_Buffer[m_BufferPtr]);
          char *pStringBuffer = ((char *)&m_Buffer[m_BufferPtr+pHeader->FormattedSize]);
          while(*pStringBuffer)
          {
            //Fill strings with rand32om chars within the mask
            while(*pStringBuffer)
            {
              *pStringBuffer = RAND_STR_MASK[mtRandom.randInt() % RAND_STR_MASK_LEN];
              ++pStringBuffer;
            }
            ++pStringBuffer;
          }
          
          switch(pHeader->Type)
          {
            case BIOS_INFO_TYPE:
             StCallback_BiosInfo(pHeader);
            break;
            case SYSTEM_INFO_TYPE:
             StCallback_SysInfo(pHeader);
            break;
            case BASEBOARD_INFO_TYPE:
             StCallback_MBInfo(pHeader);
            break;
            case CHASSIS_INFO_TYPE:
             StCallback_ChassisInfo(pHeader);
            break;
            case PROCESSOR_INFO_TYPE:
             StCallback_CpuInfo(pHeader);
            break;
            case CACHE_INFO_TYPE:
             StCallback_CacheInfo(pHeader);
            break;
            case PORTS_INFO_TYPE:
             StCallback_PortsInfo(pHeader);
            break;
            case SYSTEMSLOTS_INFO_TYPE:
             StCallback_SystemSlotsInfo(pHeader);
            break;
            case ONBOARDDEVS_INFO_TYPE:
             StCallback_OnBoardDevsInfo(pHeader);
            break;
            case OEMSTRING_INFO_TYPE:
             StCallback_OemStringsInfo(pHeader);
            break;
            case SYSTEMCONFIG_INFO_TYPE:
             StCallback_SysConfigInfo(pHeader);
            break;
            case BIOSLANG_INFO_TYPE:
             StCallback_BiosLangInfo(pHeader);
            break;
            case GROUPASSOCS_INFO_TYPE:
             StCallback_GroupAssocsInfo(pHeader);
            break;
            case SYSLOG_INFO_TYPE:
             StCallback_SysLogInfo(pHeader);
            break;
            case PHYSMEM_INFO_TYPE:
             StCallback_PhysMemInfo(pHeader);
            break;
            case MEMDEV_INFO_TYPE:
             StCallback_MemDevInfo(pHeader);
            break;
            case MEMERROR32_INFO_TYPE:
             StCallback_MemError32Info(pHeader);
            break;
            case MEMARRAYMAPPED_INFO_TYPE:
             StCallback_MemArrayMappedInfo(pHeader);
            break;
            case MEMDEVMAPPED_INFO_TYPE:
             StCallback_MemDevMappedInfo(pHeader);
            break;
            case BUILTINPTRDEV_INFO_TYPE:
             StCallback_BuiltInPtrDevInfo(pHeader);
            break;
            case BATTERY_INFO_TYPE:
             StCallback_BatteryInfo(pHeader);
            break;
            case SYSRESET_INFO_TYPE:
             StCallback_SysResetInfo(pHeader);
            break;
            case HARDSEC_INFO_TYPE:
             StCallback_HardwareSecurityInfo(pHeader);
            break;
            case SYSPOWER_INFO_TYPE:
             StCallback_SysPowerInfo(pHeader);
            break;
            case VOLTPROBE_INFO_TYPE:
             StCallback_VoltageProbeInfo(pHeader);
            break;
            case COOLINGDEV_INFO_TYPE:
             StCallback_CoolingDevInfo(pHeader);
            break;
            case TEMPPROBE_INFO_TYPE:
             StCallback_TempProbeInfo(pHeader);
            break;
            case ELECPROBE_INFO_TYPE:
             StCallback_ElectricalProbeInfo(pHeader);
            break;
            case OOBRA_INFO_TYPE:
             StCallback_OobRemoteAccessInfo(pHeader);
            break;
            case SYSBOOT_INFO_TYPE:
             StCallback_SysBootInfo(pHeader);
            break;
            case MEMERROR64_INFO_TYPE:
             StCallback_MemError64Info(pHeader);
            break;
            case MNGDEV_INFO_TYPE:
             StCallback_ManageDevInfo(pHeader);
            break;
            case MNGDEVCOMP_INFO_TYPE:
             StCallback_ManageDevCompInfo(pHeader);
            break;
            case MNGDEVTHRES_INFO_TYPE:
             StCallback_ManageDevThresholdInfo(pHeader);
            break;
            case MEMCHAN_INFO_TYPE:
             StCallback_MemChannelInfo(pHeader);
            break;
            case IPMIDEV_INFO_TYPE:
             StCallback_IpmiDevInfo(pHeader);
            break;
            case POWERSUPPLY_INFO_TYPE:
             StCallback_PowerSupplyInfo(pHeader);
            break;
            case ADDITIONAL_INFO_TYPE:
             StCallback_AdditionalInfo(pHeader);
            break;
            case ONBOARDDEVSEX_INFO_TYPE:
             StCallback_OnBoardDevExInfo(pHeader);
            break;
            case MNGCTRLHOSTIF_INFO_TYPE:
             StCallback_ManageControlHostInterfaceInfo(pHeader);
            break;
          }
          
          m_BufferPtr+=currentStLen;
        }

        
      }
      
    private:
      int32 GetStructureLen( void )
      {
        uint16 Offset = m_BufferPtr;
        uint16 BufferLen = m_BufferSize;
        StructureHeader* pHeader = ((StructureHeader *)&m_Buffer[m_BufferPtr]);
        
        Offset+=pHeader->FormattedSize;
        
        while(Offset < BufferLen)
          if(!memcmp(&m_Buffer[Offset], SMBIOS_STRUCTURE_SEPARATOR, SMBIOS_STRUCTURE_SEPARATOR_SIZE))
            return Offset-m_BufferPtr+SMBIOS_STRUCTURE_SEPARATOR_SIZE;
          else ++Offset;
        
        return -1;
      }
      
      //BIOS_INFO_TYPE:
      void StCallback_BiosInfo(StructureHeader *Header)
      {
        uint8 rTo = Header->FormattedSize-sizeof(uint32);
        *((uint16 *)(Header->Data + 0x02)) = mtRandom.randInt() & 0xFFFF;
        Header->Data[0x05] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x06)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0A)) = mtRandom.randInt();
        for(uint8 i = 0x0E; i < rTo; ++i)
          Header->Data[i] = mtRandom.randInt() & 0xFF;
      } 
      
      //SYSTEM_INFO_TYPE:
      void StCallback_SysInfo(StructureHeader *Header)
      {
        if(Header->FormattedSize < 0x19)
          return;
          
        *((uint32 *)(Header->Data + 0x04)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x08)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0C)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x10)) = mtRandom.randInt();
        Header->Data[0x14] = mtRandom.randInt() & 0xFF;
      } 
      
      //BASEBOARD_INFO_TYPE:
      void StCallback_MBInfo(StructureHeader *Header)
      {
        uint8 rTo = Header->FormattedSize-sizeof(uint32);
        Header->Data[0x05] = mtRandom.randInt() & 0xFF;
        *((uint16 *)(Header->Data + 0x07)) = mtRandom.randInt() & 0xFFFF;
        Header->Data[0x09] = mtRandom.randInt() & 0xFF;
        Header->Data[0x0A] = mtRandom.randInt() & 0xFF;
        for(uint8 i = 0x0B; i < rTo; ++i)
          Header->Data[i] = mtRandom.randInt() & 0xFF;
      } 
      
      //CHASSIS_INFO_TYPE:
      void StCallback_ChassisInfo(StructureHeader *Header)
      {
        uint8 rTo = Header->FormattedSize-sizeof(uint8)-sizeof(uint32);
        Header->Data[0x01] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x05)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x09)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0D)) = mtRandom.randInt();
        for(uint8 i = 0x11; i < rTo; ++i)
          Header->Data[i] = mtRandom.randInt() & 0xFF;
      } 
      
      //PROCESSOR_INFO_TYPE:
      void StCallback_CpuInfo(StructureHeader *Header)
      {
        *((uint16 *)(Header->Data + 0x01)) = mtRandom.randInt() & 0xFFFF;
        *((uint32 *)(Header->Data + 0x04)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x08)) = mtRandom.randInt();
        Header->Data[0x0D] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x0E)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x12)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x16)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x1A)) = mtRandom.randInt() & 0xFFFF;
        Header->Data[0x1F] = mtRandom.randInt() & 0xFF;
        *((uint16 *)(Header->Data + 0x20)) = mtRandom.randInt() & 0xFFFF;
        *((uint32 *)(Header->Data + 0x22)) = mtRandom.randInt();
      } 
      
      //CACHE_INFO_TYPE:
      void StCallback_CacheInfo(StructureHeader *Header)
      {
        *((uint32 *)(Header->Data + 0x01)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x05)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x09)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x0D)) = mtRandom.randInt() & 0xFFFF;
      } 
      
      //PORTS_INFO_TYPE:
      void StCallback_PortsInfo(StructureHeader *Header)
      {
        Header->Data[0x01] = mtRandom.randInt() & 0xFF;
        *((uint16 *)(Header->Data + 0x03)) = mtRandom.randInt() & 0xFFFF;
      } 
      
      //SYSTEMSLOTS_INFO_TYPE:
      void StCallback_SystemSlotsInfo(StructureHeader *Header)
      {
        *((uint32 *)(Header->Data + 0x01)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x05)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x09)) = mtRandom.randInt();
      } 
      
      //ONBOARDDEVS_INFO_TYPE:
      void StCallback_OnBoardDevsInfo(StructureHeader *Header)
      {
        uint8 devCount = (Header->FormattedSize - sizeof(uint32))/sizeof(uint16);
        for(uint8 i = 0; i < devCount; ++i)
          Header->Data[2*i] = mtRandom.randInt() & 0xFF;
      } 
      
      //OEMSTRING_INFO_TYPE:
      void StCallback_OemStringsInfo(StructureHeader *Header)
      {//Nothing to do here
      } 
      
      //SYSTEMCONFIG_INFO_TYPE:
      void StCallback_SysConfigInfo(StructureHeader *Header)
      {//Nothing to do here
      } 
      
      //BIOSLANG_INFO_TYPE:
      void StCallback_BiosLangInfo(StructureHeader *Header)
      {
        Header->Data[0x01] = mtRandom.randInt() & 0x01;
      } 
      
      //GROUPASSOCS_INFO_TYPE:
      void StCallback_GroupAssocsInfo(StructureHeader *Header)
      {
        uint8 rTo = Header->FormattedSize-sizeof(uint32);
        for(uint8 i = 0x01; i < rTo; ++i)
          Header->Data[i] = mtRandom.randInt() & 0xFF;
      } 
      
      //SYSLOG_INFO_TYPE:
      void StCallback_SysLogInfo(StructureHeader *Header)
      {
        uint8 rTo = Header->FormattedSize-sizeof(uint32); 
        *((uint16 *)(Header->Data + 0x06)) = mtRandom.randInt() & 0xFFFF;
        *((uint32 *)(Header->Data + 0x08)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0C)) = mtRandom.randInt();
        Header->Data[0x10] = mtRandom.randInt() & 0xFF;
        for(uint8 i = 0x13; i < rTo; ++i)
          Header->Data[i] = mtRandom.randInt() & 0xFF;
      } 
      
      //PHYSMEM_INFO_TYPE:
      void StCallback_PhysMemInfo(StructureHeader *Header)
      {
        Header->Data[0x00] = mtRandom.randInt() & 0xFF;
        *((uint16 *)(Header->Data + 0x01)) = mtRandom.randInt() & 0xFFFF;
        *((uint32 *)(Header->Data + 0x03)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x09)) = mtRandom.randInt() & 0xFFFF;
        *((uint32 *)(Header->Data + 0x0B)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0F)) = mtRandom.randInt();
      } 
      
      //MEMDEV_INFO_TYPE:
      void StCallback_MemDevInfo(StructureHeader *Header)
      {
        *((uint32 *)(Header->Data + 0x04)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x08)) = mtRandom.randInt();
        Header->Data[0x0E] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x0F)) = mtRandom.randInt();
        Header->Data[0x17] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x18)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x1C)) = mtRandom.randInt() & 0xFFFF;
      } 
      
      //MEMERROR32_INFO_TYPE:
      void StCallback_MemError32Info(StructureHeader *Header)
      {
        Header->Data[0x00] = mtRandom.randInt() & 0xFF;
        *((uint16 *)(Header->Data + 0x01)) = mtRandom.randInt() & 0xFFFF;
        *((uint32 *)(Header->Data + 0x03)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x07)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0B)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0F)) = mtRandom.randInt();
      } 
      
      //MEMARRAYMAPPED_INFO_TYPE:
      void StCallback_MemArrayMappedInfo(StructureHeader *Header)
      {
        *((uint32 *)(Header->Data + 0x00)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x04)) = mtRandom.randInt();
        Header->Data[0x0A] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x0B)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0F)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x13)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x17)) = mtRandom.randInt();
      } 
      
      //MEMDEVMAPPED_INFO_TYPE:
      void StCallback_MemDevMappedInfo(StructureHeader *Header)
      {
        *((uint32 *)(Header->Data + 0x00)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x04)) = mtRandom.randInt();
        Header->Data[0x0C] = mtRandom.randInt() & 0xFF;
        *((uint16 *)(Header->Data + 0x0D)) = mtRandom.randInt() & 0xFFFF;
        *((uint32 *)(Header->Data + 0x0F)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x13)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x17)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x1B)) = mtRandom.randInt();
      } 
      
      //BUILTINPTRDEV_INFO_TYPE:
      void StCallback_BuiltInPtrDevInfo(StructureHeader *Header)
      {
        *((uint16 *)(Header->Data + 0x00)) = mtRandom.randInt() & 0xFFFF; 
      } 
      
      //BATTERY_INFO_TYPE:
      void StCallback_BatteryInfo(StructureHeader *Header)
      {
        Header->Data[0x05] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x06)) = mtRandom.randInt();
        Header->Data[0x0B] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x0C)) = mtRandom.randInt();
        Header->Data[0x11] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x12)) = mtRandom.randInt();
      } 
      
      //SYSRESET_INFO_TYPE:
      void StCallback_SysResetInfo(StructureHeader *Header)
      {
        Header->Data[0x00] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x01)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x05)) = mtRandom.randInt();
      } 
      
      //HARDSEC_INFO_TYPE:
      void StCallback_HardwareSecurityInfo(StructureHeader *Header)
      {
        Header->Data[0x00] = mtRandom.randInt() & 0xFF;
      } 
      
      //SYSPOWER_INFO_TYPE:
      void StCallback_SysPowerInfo(StructureHeader *Header)
      {
        Header->Data[0x00] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x01)) = mtRandom.randInt();
      } 
      
      //VOLTPROBE_INFO_TYPE:
      void StCallback_VoltageProbeInfo(StructureHeader *Header)
      {
        Header->Data[0x01] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x02)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x06)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0A)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0E)) = mtRandom.randInt();
      } 
      
      //COOLINGDEV_INFO_TYPE:
      void StCallback_CoolingDevInfo(StructureHeader *Header)
      {
        *((uint16 *)(Header->Data + 0x02)) = mtRandom.randInt() & 0xFFFF;
        *((uint32 *)(Header->Data + 0x04)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x08)) = mtRandom.randInt() & 0xFFFF;
      } 
      
      //TEMPPROBE_INFO_TYPE:
      void StCallback_TempProbeInfo(StructureHeader *Header)
      {
        Header->Data[0x01] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x02)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x06)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x0A)) = mtRandom.randInt() & 0xFFFF;
        *((uint32 *)(Header->Data + 0x0C)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x10)) = mtRandom.randInt() & 0xFFFF;
      } 
      
      //ELECPROBE_INFO_TYPE:
      void StCallback_ElectricalProbeInfo(StructureHeader *Header)
      {
        Header->Data[0x01] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x02)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x06)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x0A)) = mtRandom.randInt() & 0xFFFF;
        *((uint32 *)(Header->Data + 0x0C)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x10)) = mtRandom.randInt() & 0xFFFF;
      } 
      
      //OOBRA_INFO_TYPE:
      void StCallback_OobRemoteAccessInfo(StructureHeader *Header)
      {
        Header->Data[0x01] = mtRandom.randInt() & 0xFF;
      } 
      
      //SYSBOOT_INFO_TYPE:
      void StCallback_SysBootInfo(StructureHeader *Header)
      {
        *((uint32 *)(Header->Data + 0x06)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0A)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x0A)) = mtRandom.randInt() & 0xFFFF;
      } 
      
      //MEMERROR64_INFO_TYPE:
      void StCallback_MemError64Info(StructureHeader *Header)
      {
        Header->Data[0x00] = mtRandom.randInt() & 0xFF;
        *((uint16 *)(Header->Data + 0x01)) = mtRandom.randInt() & 0xFFFF;
        *((uint32 *)(Header->Data + 0x03)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x07)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0B)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0F)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x13)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x17)) = mtRandom.randInt();
      } 
      
      //MNGDEV_INFO_TYPE:
      void StCallback_ManageDevInfo(StructureHeader *Header)
      {
        Header->Data[0x01] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x02)) = mtRandom.randInt();
        Header->Data[0x06] = mtRandom.randInt() & 0xFF;
      } 
      
      //MNGDEVCOMP_INFO_TYPE:
      void StCallback_ManageDevCompInfo(StructureHeader *Header)
      {//Nothing to do
      } 
      
      //MNGDEVTHRES_INFO_TYPE:
      void StCallback_ManageDevThresholdInfo(StructureHeader *Header)
      {
        *((uint32 *)(Header->Data + 0x00)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x04)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x08)) = mtRandom.randInt();
      } 
      
      //MEMCHAN_INFO_TYPE:
      void StCallback_MemChannelInfo(StructureHeader *Header)
      {
        uint8 DevCount = (Header->FormattedSize-sizeof(uint32)-sizeof(uint16)-sizeof(uint8))/(sizeof(uint16) + sizeof(uint8));
        *((uint16 *)(Header->Data + 0x00)) = mtRandom.randInt() & 0xFFFF;
        for(uint8 i = 0x00; i < DevCount; ++i)
          Header->Data[0x03 + 3*i] = mtRandom.randInt() & 0xFF;
      } 
      
      //IPMIDEV_INFO_TYPE:
      void StCallback_IpmiDevInfo(StructureHeader *Header)
      {
        *((uint32 *)(Header->Data + 0x00)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x04)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x08)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x0C)) = mtRandom.randInt() & 0xFFFF;
      } 
      
      //POWERSUPPLY_INFO_TYPE:
      void StCallback_PowerSupplyInfo(StructureHeader *Header)
      {
        Header->Data[0x00] = mtRandom.randInt() & 0xFF;
        *((uint32 *)(Header->Data + 0x08)) = mtRandom.randInt();
        *((uint32 *)(Header->Data + 0x0C)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x10)) = mtRandom.randInt() & 0xFFFF;
      } 
      
      //ADDITIONAL_INFO_TYPE:
      void StCallback_AdditionalInfo(StructureHeader *Header)
      {//Fomart has not a fix std, so we don't modify it
      } 
      
      //ONBOARDDEVSEX_INFO_TYPE:
      void StCallback_OnBoardDevExInfo(StructureHeader *Header)
      {
        *((uint32 *)(Header->Data + 0x01)) = mtRandom.randInt();
        *((uint16 *)(Header->Data + 0x05)) = mtRandom.randInt() & 0xFFFF;
      } 
      
      //MNGCTRLHOSTIF_INFO_TYPE:
      void StCallback_ManageControlHostInterfaceInfo(StructureHeader *Header)
      {
        Header->Data[0x00] = mtRandom.randInt() & 0xFF;
      }
      
    protected:
      uint8 *m_Buffer;
      uint32 m_BufferPtr;
      uint32 m_BufferSize;
  };
};




//Unban specific functions
uint8 *g_XnaddrData = NULL;

void Unban_NullName( void )
{
  //Change Name to nothing
  DWORD dwProtection = PAGE_EXECUTE_READWRITE;
  LPBYTE pPlayerInfo = LPBYTE(*LPDWORD(ADDRESS_PLAYER_INFO));
  if(pPlayerInfo)
  {
    LPBYTE pPlayerName = LPBYTE(pPlayerInfo+0x142);
    uint8 NameLen = 4 + mtRandom.randInt() % PLAYER_NAME_MAXLEN;
    VirtualProtect(pPlayerName, NameLen+1, dwProtection, &dwProtection);
    memset(pPlayerName, 0x09, NameLen); pPlayerName[NameLen] = 0;
    VirtualProtect(pPlayerName, NameLen+1, dwProtection, &dwProtection);
  }
}

void Unban_ChangeXuid( void )
{
  //Change XUID
  *LPDWORD(ADDRESS_XUID) = mtRandom.randInt();
}

void Unban_ChangeXnaddr( void )
{
  //Change XNADDR
  for(uint8 i = 0; i < XNADDR_LEN;)
    g_XnaddrData[i++] = mtRandom.randInt() & 0xFF;
}

void Unban_ChangeSteamId( void )
{
  LPBYTE pPlayerInfo = LPBYTE(*LPDWORD(ADDRESS_PLAYER_INFO));
  if(pPlayerInfo)
  {
   LPDWORD pSteamId = LPDWORD(pPlayerInfo+0x13A);
   pSteamId[0] = mtRandom.randInt();
  }
}

UINT GetSystemFirmwareTableReal
(
  DWORD FirmwareTableProviderSignature,
  DWORD FirmwareTableID,
  PVOID pFirmwareTableBuffer,
  DWORD BufferSize
)
{
  ULONG uReturnedLen = 0;
  LPBYTE pBuffer = LPBYTE(MALLOC(BufferSize+0x10));
  *LPDWORD(&pBuffer[0x00]) = FirmwareTableProviderSignature;
  *LPDWORD(&pBuffer[0x04]) = 0x00000001;
  *LPDWORD(&pBuffer[0x08]) = FirmwareTableID;
  *LPDWORD(&pBuffer[0x0C]) = BufferSize;

  LONG fnRet = ZwQuerySystemInformation(SYSTEMINFO_CLASS_FIRM_TABLE, pBuffer, BufferSize+0x10, NULL);

  uReturnedLen = *LPDWORD(&pBuffer[0x0C]);
  if(fnRet < 0)
  {
    if(fnRet != 0xC0000023)
      uReturnedLen = 0;
  }
  else
    memcpy(pFirmwareTableBuffer, &pBuffer[0x10], uReturnedLen);

  FREE(pBuffer);
  return uReturnedLen;
}

UINT Hook_GetSystemFirmwareTable
(
  DWORD FirmwareTableProviderSignature,
  DWORD FirmwareTableID,
  PVOID pFirmwareTableBuffer,
  DWORD BufferSize
)
{
  UINT fnReturn = GetSystemFirmwareTableReal(FirmwareTableProviderSignature, FirmwareTableID, pFirmwareTableBuffer, BufferSize);

  if(BufferSize && fnReturn)
  {
    Unban_NullName();
    Unban_ChangeXuid();
    Unban_ChangeXnaddr();
    Unban_ChangeSteamId();
    //Change SMBIOS Info
    SMBIOS::AlterInfo *alterSmBios = new SMBIOS::AlterInfo((uint8 *)pFirmwareTableBuffer, fnReturn);
    alterSmBios->Process();    
    delete alterSmBios;
  }

  return fnReturn;
}

void Hook( void )
{
  DWORD dwProtection = PAGE_EXECUTE_READWRITE;
  LPBYTE pHookAddress = LPBYTE(GetProcAddress(GetModuleHandleA("kernel32.dll"), "GetSystemFirmwareTable"));
  ZwQuerySystemInformation = pZwQuerySystemInformation(GetProcAddress(GetModuleHandleA("ntdll.dll"), "ZwQuerySystemInformation"));

  LPBYTE pTargetAddress = LPBYTE(Hook_GetSystemFirmwareTable);
  VirtualProtect(pHookAddress, JMP_HOOK_SIZE, dwProtection, &dwProtection);
  *pHookAddress = 0xE9; *LPDWORD(pHookAddress + 1) = pTargetAddress-pHookAddress-JMP_HOOK_SIZE;
  VirtualProtect(pHookAddress, JMP_HOOK_SIZE, dwProtection, &dwProtection);

  //Zero fill unnamed player name string
  dwProtection = PAGE_EXECUTE_READWRITE;
  LPBYTE pUnnamedPlayerName = LPBYTE(ADDRESS_UNNAMEDPLAYER_NAME);
  VirtualProtect(pUnnamedPlayerName, sizeof(uint8), dwProtection, &dwProtection);
  *pUnnamedPlayerName = 0;
  VirtualProtect(pUnnamedPlayerName, sizeof(uint8), dwProtection, &dwProtection);

  //Change XNADDR Buffer
  if(g_XnaddrData == NULL)
    g_XnaddrData = ((uint8 *)MALLOC(XNADDR_LEN));

  LPDWORD pXnAddrBuffer = LPDWORD(ADDRESS_XNADDRESS_BUFFER);
  VirtualProtect(pXnAddrBuffer, sizeof(uint32), dwProtection, &dwProtection);
  *pXnAddrBuffer = DWORD(g_XnaddrData);
  VirtualProtect(pXnAddrBuffer, sizeof(uint32), dwProtection, &dwProtection);

  Unban_ChangeXuid();
  Unban_ChangeXnaddr();
  Unban_ChangeSteamId();
}

BOOL APIENTRY DllMain(HMODULE, DWORD callReason, LPVOID)
{
  static bool bLoaded = false;
  if(callReason == DLL_PROCESS_ATTACH && !bLoaded)
  {
     Hook();
     bLoaded = true;
  }

    return TRUE;
}